class Stepper {
  constructor(selector) {
    this.steppers = document.querySelectorAll(selector);

    this.init();
  }

  init() {
    this.addListener();
  }

  addListener() {
    const self = this;

    [].forEach.call(this.steppers, stepper => {
      [].forEach.call(stepper.querySelectorAll(".step"), step => {
        const prevButton = step.querySelector(".prev");
        const nextButton = step.querySelector(".next");

        if (prevButton) {
          prevButton.addEventListener("click", function() {
            self.goPrev(this);
          });
        }
        if (nextButton) {
          nextButton.addEventListener("click", function() {
            self.goNext(this);
          });
        }
      });
    });
  }

  goPrev(triggerButton) {
    const step = triggerButton.closest(".step");

    step.classList.remove("active");
    step.previousElementSibling.classList.add("active");
  }

  goNext(triggerButton) {
    const step = triggerButton.closest(".step");

    step.classList.remove("active");
    step.nextElementSibling.classList.add("active");
  }
}

export default Stepper;
